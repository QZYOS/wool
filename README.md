# wool

#### 介绍
羊毛党

一款观看视频的APP，可以看各大平台视频。

#### 安装教程

1.  `cd wool`
2.  Android `flutter build apk --release`
3.  iOS `flutter build ios --release`

下载二维码：
![a](./5a203692bd4c74d1c7710e32a65c3fd6.png)

#### 声明
本项目仅供学习参考使用，任何商用行为与本项目无关

