import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:wool/utils/screen_util.dart';

/// 颜色创建方法
/// - [colorString] 颜色值
/// - [alpha] 透明度(默认1，0-1)
///
/// 可以输入多种格式的颜色代码，如: 0x000000,0xff000000,#000000
AJColor(value, {double alpha = 1.0}) {
  String colorStr = value.toString();

  if (!colorStr.startsWith('0xff') && colorStr.length == 6) {
    // value未带0xff前缀并且长度为6，如000000
    colorStr = '0xff' + colorStr;
  } else if (colorStr.startsWith('0x') && colorStr.length == 8) {
    // value为8位，如0x000000
    colorStr = colorStr.replaceRange(0, 2, '0xff');
  } else if (colorStr.startsWith('#') && colorStr.length == 7) {
    // value为7位，如#000000
    colorStr = colorStr.replaceRange(0, 1, '0xff');
  } else if (colorStr.startsWith('0xff') && colorStr.length == 10) {
    // value为10位，如0xff000000，不作处理
    // colorStr = colorStr.replaceRange(0, 1, '0xff');
  } else {
    return Colors.black;
  }
  // 先分别获取色值的RGB通道
  Color color = Color(int.parse(colorStr));
  int red = color.red;
  int green = color.green;
  int blue = color.blue;
  // 通过fromRGBO返回带透明度和RGB值的颜色
  return Color.fromRGBO(red, green, blue, alpha);
}


//  根据iphone 6 的标准宽度比例获取真实对应的尺寸
double TrueSizeW(double size) {
  setDesignWHD(375, 667);
  return ScreenUtil.getInstance().getWidth(size);
}

//  根据iphone 6 的标准宽度; 适配小于iphone6宽度的尺寸；大于iPhone6宽度保持原数据
double TrueSizeMinW(double size) {
  double trueW = ScreenUtil.getInstance().getWidth(size);
  return (trueW > size) ? size : trueW;
}

//  根据iphone 6 的标准高度比例获取真实对应的尺寸
double TrueSizeH(double size) {
  setDesignWHD(375, 667);
  return ScreenUtil.getInstance().getHeight(size);
}

double TrueFontSize(double size) {
  setDesignWHD(375, 667);
  return ScreenUtil.getInstance().getSp(size);
}

//延迟执行 默认100毫秒
void Delay({int milliseconds = 100, @required Function block}) {
  Future.delayed(Duration(milliseconds: milliseconds)).whenComplete(() {
    block();
  });
}
