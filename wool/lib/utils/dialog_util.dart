import 'package:flutter/material.dart';
import 'package:wool/common/aj_style.dart';
import 'package:wool/common/max_scale_text_widget.dart';
import 'package:wool/widget/aj_input_widget.dart';

class DialogUtil{
  static Future<bool> dialogWithTextField(
      BuildContext context,
      String title, {
        VoidCallback confirmTap,
        TextEditingController controller,
        TextEditingController controller1,
        String hintText = "请输入站点",
        String hintText1 = "请输入站点url",
        int maxLength = 100,
      }) {
    double _btnH = 50.0;
    double _h = 280.0;
    double _w = 300.0;

    return showDialog(
      context: context,
      builder: (context) {
        return GestureDetector(
          onTap: () => //隐藏键盘
          FocusScope.of(context).requestFocus(new FocusNode()),
          child: Material(
            color: Colors.transparent,
            child: MaxScaleTextWidget(
              child: WillPopScope(
                onWillPop: () async => false,
                child: Center(
                  child: Container(
                    width: _w,
                    height: _h,
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    ),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Text(
                            title,
                            style: AJStyle.summarizeDescStyle,
                          ),
                          margin: EdgeInsets.only(
                              top: 16, left: 8, right: 8, bottom: 16),
                        ),
                        AJInputWidget(
                          hintText: hintText,
                          margin: EdgeInsets.fromLTRB(16, 0, 16, 8),
                          isUnderline: false,
                          maxLines: 1,
                          outlineBorderRadius: 4,
                          controller: controller,
                          fontSize: AJFont.textSize16,
                        ),
                        AJInputWidget(
                          hintText: hintText1,
                          margin: EdgeInsets.fromLTRB(16, 0, 16, 8),
                          isUnderline: false,
                          maxLines: 1,
                          outlineBorderRadius: 4,
                          controller: controller1,
                          fontSize: AJFont.textSize16,
                        ),
                        Expanded(child: Container()),
                        Row(
                          children: <Widget>[
                            Expanded(
                                child: new InkWell(
                                  onTap: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      border: Border(
                                        top: BorderSide(color: AJColors.app_line),
                                        right: BorderSide(color: AJColors.app_line),
                                      ),
                                    ),
                                    height: _btnH,
                                    alignment: Alignment.center,
                                    child: new Text(
                                      "取消",
                                      style: AJStyle.summarizeDescStyle,
                                    ),
                                  ),
                                )),
                            Expanded(
                                child: new InkWell(
                                  onTap: () {
                                    Navigator.of(context).pop();
                                    if (confirmTap != null) {
                                      confirmTap();
                                    }
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      border: Border(
                                        left: BorderSide(color: AJColors.app_line),
                                        top: BorderSide(color: AJColors.app_line),
                                      ),
                                    ),
                                    height: _btnH,
                                    alignment: Alignment.center,
                                    child: new Text(
                                      "提交",
                                      style: AJStyle.summarizeDescStyle,
                                    ),
                                  ),
                                ))
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
      barrierDismissible: false,
    );
  }

  /// 单击提示退出
  static Future<bool> dialogConfigRegisterApp(
      BuildContext context, String title,
      {VoidCallback confirmTap, VoidCallback cancelTap}) {
    double _btnH = 50.0;
    double _h = 145.0;
    double _w = 312.0;

    return showDialog(
      context: context,
      builder: (context) {
        return Material(
          color: Colors.transparent,
          child: MaxScaleTextWidget(
              child: WillPopScope(
                onWillPop: () async => false,
                child: Center(
                  child: Container(
                    width: _w,
                    height: _h,
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      //用一个BoxDecoration装饰器提供背景图片
                      borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    ),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                          child: Text(
                            title,
                            style: AJStyle.summarizeDescStyle,
                          ),
                          margin: EdgeInsets.only(top: 30, left: 10, right: 10),
                        ),
                        Expanded(child: Container()),
                        Row(
                          children: <Widget>[
                            Expanded(
                                child: new InkWell(
                                  onTap: () {
                                    Navigator.of(context).pop();
                                    if (cancelTap != null) {
                                      cancelTap();
                                    }
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      border: Border(
                                        top: BorderSide(color: AJColors.app_line),
                                        right: BorderSide(color: AJColors.app_line),
                                      ),
                                    ),
                                    height: _btnH,
                                    alignment: Alignment.center,
                                    child: new Text(
                                      "取消",
                                      style: AJStyle.summarizeDescStyle,
                                    ),
                                  ),
                                )),
                            Expanded(
                                child: new InkWell(
                                  onTap: () {
                                    Navigator.of(context).pop();
                                    if (confirmTap != null) {
                                      confirmTap();
                                    }
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      border: Border(
                                        left: BorderSide(color: AJColors.app_line),
                                        top: BorderSide(color: AJColors.app_line),
                                      ),
                                    ),
                                    height: _btnH,
                                    alignment: Alignment.center,
                                    child: new Text(
                                      "确定",
                                      style: AJStyle.summarizeDescStyle,
                                    ),
                                  ),
                                ))
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              )),
        );
      },
      barrierDismissible: false,
    );
  }
}


