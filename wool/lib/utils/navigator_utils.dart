import 'package:flutter/material.dart';
import 'package:wool/common/max_scale_text_widget.dart';


class NavigatorUtils {

  /// push  child 要跳转的页面     needTransition是否需要动画
  static pushTO(BuildContext context,
      {Widget child, bool needTransition = false}) {
    return _push(context, child: child, needTransition: needTransition);
  }

  /// pushPageRouteBuilderTO
  static pushPageRouteBuilderTO(BuildContext context, {Widget child}) {
    return Navigator.pushAndRemoveUntil(
        context,
        new PageRouteBuilder(
          opaque: false,
          pageBuilder: (BuildContext context, _, __) {
            return MaxScaleTextWidget(
              child: child,
            );
          },
        ),
        (Route<dynamic> route) => false);
  }

  static pushNotAnimationTO(BuildContext context, {Widget child}) {
    return Navigator.push(
      context,
      PageRouteBuilder(
        opaque: true,
        pageBuilder: (BuildContext context, _, __) {
          return MaxScaleTextWidget(
            child: child,
          );
        },
      ),
    );
  }

  //base push 基类
  static _push(BuildContext context,
      {Widget child, bool needTransition = false}) {
    if (needTransition) {
      return Navigator.push(
        context,
        new PageRouteBuilder(
            pageBuilder: (BuildContext context, Animation<double> animation,
                Animation<double> secondaryAnimation) {
              return MaxScaleTextWidget(
                child: child,
              );
            },
            transitionDuration: const Duration(milliseconds: 200),
            transitionsBuilder: (BuildContext context,
                Animation<double> animation,
                Animation<double> secondaryAnimation,
                Widget child) {
              // 添加一个平移动画
              return createTransition(animation, child);
            }),
      );
    } else {
      return Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => MaxScaleTextWidget(
                  child: child,
                )),
      );

    }
  }

  static SlideTransition createTransition(
      Animation<double> animation, Widget child,
      {bool reverse = false}) {
    double offset = reverse ? -1.0 : 1.0;
    return new SlideTransition(
      position: new Tween<Offset>(
        begin: Offset(offset, 0.0),
        end: Offset(0.0, 0.0),
      ).animate(animation),
      child: child, // child is the value returned by pageBuilder
    );
  }
}
