import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:wool/common/aj_crash_collect.dart';
import 'package:wool/common/localizations.dart';
import 'package:wool/page/home/home_page.dart';
import 'package:wool/page/main_page/launch_page.dart';

void main() {
  AJCrashCollect.init(() {
    runApp(new MyApp());
  });
}
final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

class MyApp extends StatelessWidget {
  List<Locale> an = [
    const Locale( 'zh', 'CH' ),
    const Locale( 'en', 'US' ),
  ];
  List<Locale> ios = [
    const Locale( 'en', 'US' ),
    const Locale( 'zh', 'CH' ),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigatorKey,
      debugShowCheckedModeBanner: false,
      supportedLocales: Platform.isIOS ? ios : an,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        ChineseCupertinoLocalizations.delegate,
      ],
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LaunchPage( ),
      routes: {
        HomePage.sName: (context) {
          return new HomePage( );
        }
      },
      builder: EasyLoading.init(),

    );
  }
}

