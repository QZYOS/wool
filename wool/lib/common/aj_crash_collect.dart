import 'dart:async';

import 'package:flutter/material.dart';
import 'package:wool/utils/aj_log_util.dart';

class AJCrashCollect{
  static void init<R>(R body()){
    AJLogUtil.init(tag: "lsdclient log");
    FlutterError.onError = (FlutterErrorDetails details) {
      // 上报错误和日志逻辑
      _reportErrorAndLog(details);
    };
    runZoned(
          () => body(),
//      zoneSpecification: ZoneSpecification(
//        print: (Zone self, ZoneDelegate parent, Zone zone, String line) {
//          _collectLog(line); //  收集日志
//        },
//      ),
      onError: (Object obj, StackTrace stack) {
        //  构建错误信息
        var details = _makeDetails(obj, stack);
        // 上报错误和日志逻辑
        _reportErrorAndLog(details);
      },
    );
  }

  static void _collectLog(String line) {
    // 收集日志
    print("_collectLog");
//    print(line);
  }

  static void _reportErrorAndLog(FlutterErrorDetails details) {
    // 上报错误和日志逻辑
//    print(details.toString());
    AJLogUtil.v(details.toString(), tag: "_reportErrorAndLog -------->");
  }

  static FlutterErrorDetails _makeDetails(Object obj, StackTrace stack) {
    //  构建错误信息
    AJLogUtil.v("$obj", tag: "_makeDetails_obj");
    AJLogUtil.v(stack.toString(), tag: "_makeDetails_stack -------- >");
  }
}