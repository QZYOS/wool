import 'dart:math';

import 'package:flutter/material.dart';

class MaxScaleTextWidget extends StatelessWidget {
  final double max;
  final Widget child;

  MaxScaleTextWidget({Key key, this.max = 1.0, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var data = MediaQuery.of(context);
    var scal = min(max, data.textScaleFactor);
    return MediaQuery(data: data.copyWith(textScaleFactor: scal), child: child);
  }
}

class FixedSizeText extends Text {
  const FixedSizeText(
    String data, {
    Key key,
    TextStyle style,
    StrutStyle strutStyle,
    TextAlign textAlign,
    TextDirection textDirection,
    Locale locale,
    bool softWrap,
    TextOverflow overflow,
    double textScaleFactor = 1,
    int maxLines,
    String semanticsLabel,
  }) : super(data,
            key: key,
            style: style,
            strutStyle: strutStyle,
            textAlign: textAlign,
            textDirection: textDirection,
            locale: locale,
            softWrap: softWrap,
            overflow: overflow,
            textScaleFactor: textScaleFactor,
            maxLines: maxLines,
            semanticsLabel: semanticsLabel);
}
