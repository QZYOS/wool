import 'package:flutter/material.dart';
import 'package:wool/utils/screen_util.dart';

class AJColors {
  //0xFF24292E  十六进制颜色值   0xFF 表示透明度100% 24292E表示对应颜色值  0xFF19234D
  static const int primaryValue = 0xFFffffff;
  static const Color backgroundColor = Color(0xFFF3F3F8);
  static const Color primaryColor = Color(0xFFF3F3F8);
  static const Color app_line = Color(0xffEBEBEB);
  static const Color summarizeDescColor = Color(0xFF949CB4);
  static const Color blackColor = Color(0xff030303);
  static const Color navColor = Color(0xFF7367F0);


}


class AJICons {
  static const String arrow_left = 'assets/images/arrow_left.png';
  static const IconData arrow_back = Icons.arrow_back;

  //更多按钮
  static const IconData more_horiz = Icons.more_horiz;
}

class AJFont {
  //输入框字体大小
//项目名称字体
  static const textSize8 = 8.0;
  static const textSize9 = 9.0;
  static const textSize10 = 10.0;
  static const textSize12 = 12.0;
  static const textSize13 = 13.0;
  static const textSize14 = 14.0;
  static const textSize15 = 15.0;
  static const textSize16 = 16.0;
  static const textSize17 = 18.0;
  static const textSize18 = 18.0;
  static const textSize20 = 20.0;
  static const textSize24 = 24.0;
  static const textSize36 = 36.0;
}

///文本样式
class AJStyle {

  static var whiteLittleStyle = TextStyle(
    color: AJColors.primaryColor,
    fontSize: AJFont.textSize14,
  );
  static TextStyle getTextStyle({Color color, double fontSize, bool isFontWeight = false}) {
    return TextStyle(
      color: color,
      fontSize: fontSize,
      fontWeight: isFontWeight == true ? FontWeight.bold : FontWeight.normal,
    );
  }

  static var summarizeDescStyle = TextStyle(
    color: AJColors.summarizeDescColor,
    fontSize: AJFont.textSize16,
  );

  static var blackStyle = TextStyle(
    color: AJColors.blackColor,
    fontSize: AJFont.textSize16,
  );
}