import 'package:flutter/material.dart';
import 'package:wool/common/aj_style.dart';
import 'package:wool/page/web/webview_page.dart';
import 'package:wool/utils/navigator_utils.dart';
import 'package:wool/utils/screen_util.dart';
import 'package:wool/widget/item_widget.dart';

class HelpPage extends StatefulWidget {
  static final String sName = "home";

  @override
  _HelpPageState createState() => _HelpPageState();
}

class _HelpPageState extends State<HelpPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AJColors.backgroundColor,
      appBar: AppBar(
        title: Text("帮助中心"),
      ),
      body: _body(),
    );
  }

  _body() {
    return ListView(children: [
      Container(alignment: Alignment.center, child: Image.asset("assets/images/help1.png", height: ScreenUtil.getScreenH(context) * 0.85,),),
      Container(margin: EdgeInsets.only(top: 20),alignment: Alignment.center, child: Image.asset("assets/images/help2.png", height: ScreenUtil.getScreenH(context) * 0.85,),),
    ],);
  }
}
