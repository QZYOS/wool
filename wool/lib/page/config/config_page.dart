import 'dart:convert';

import 'package:common_utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:wool/common/aj_style.dart';
import 'package:wool/common/local_storage.dart';
import 'package:wool/page/web/webview_page.dart';
import 'package:wool/utils/dialog_util.dart';
import 'package:wool/utils/navigator_utils.dart';
import 'package:wool/utils/screen_util.dart';
import 'package:wool/widget/item_widget.dart';

class ConfigPage extends StatefulWidget {
  static final String sName = "home";

  @override
  _ConfigPageState createState() => _ConfigPageState();
}

class _ConfigPageState extends State<ConfigPage> {
  List _list = [];

  @override
  void initState() {
    super.initState();
    _init();
  }

  _init() async {
    _list = await LocalConfig.getLocalData();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AJColors.backgroundColor,
      appBar: AppBar(
        title: Text("设置"),
        actions: [
          IconButton(
              icon: Icon(
                Icons.add,
                color: Colors.white,
                size: 24,
              ),
              onPressed: _add),
        ],
      ),
      body: _body(),
    );
  }

  _add() {
    TextEditingController c = TextEditingController();
    TextEditingController c1 = TextEditingController();

    Future<bool> submit({bool isNeedLoading: true}) async {
      String title = c.text;
      String url = c1.text;
      if (!ObjectUtil.isEmptyString(title) && !ObjectUtil.isEmptyString(url)) {
        _list.add({"title": title, "url": url});
        await LocalConfig.saveLocalData(_list);
        _init();
      }
      return false;
    }

    DialogUtil.dialogWithTextField(
      context,
      "提问",
      controller: c,
      controller1: c1,
      confirmTap: submit,
    );
  }

  _body() {
    return Container(
      height: ScreenUtil.getScreenH(context),
      color: AJColors.backgroundColor,
      child: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildListDelegate([_content()]),
          ),
        ],
      ),
    );
  }

  _content() {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        var map = _list[index];
        return ItemWidget(
          title: "${map["title"]}",
          desc: "${map["url"]}",
          icon: Icons.clear,
          onTap: () async {
            DialogUtil.dialogConfigRegisterApp(context, "是否删除？",
                confirmTap: () async {
              _list.removeAt(index);
              await LocalConfig.saveLocalData(_list);
              _init();
            });
          },
        );
      },
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: _list.length,
    );
  }
  @override
  void dispose() {
    print("dispose");
    super.dispose();
  }
}

class LocalConfig {
  static const String CONFIG_LIST_KEY = "local.config.list";

  /// 默认数据
  static initData() {
    List _list = [
      {"title": "腾讯视频", "url": "https://m.v.qq.com"},
      {"title": "搜狐视频", "url": "https://m.tv.sohu.com"},
      {"title": "芒果视频", "url": "https://m.mgtv.com"},
      {"title": "优酷视频", "url": "https://m.youku.com"},
      {"title": "哔哩哔哩", "url": "https://m.bilibili.com"},
      {"title": "爱奇艺", "url": "https://m.iqiyi.com"},
    ];
    String s = json.encode(_list);
    return s;
  }

  /// 获取本地数据
  static getLocalData() async {
    var d = await LocalStorage.get(LocalConfig.CONFIG_LIST_KEY);
    if (d == null) {
      await LocalStorage.save(LocalConfig.CONFIG_LIST_KEY, initData());
      d = await LocalStorage.get(LocalConfig.CONFIG_LIST_KEY);
    }
    return json.decode(d);
  }

  /// 保存本地数据
  static saveLocalData(List list) async {
    String s = initData();
    if (list != null) {
      s = json.encode(list);
    }
    await LocalStorage.save(LocalConfig.CONFIG_LIST_KEY, s);
  }
}
