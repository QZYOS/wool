import 'package:flutter/material.dart';
import 'package:wool/common/aj_style.dart';
import 'package:wool/page/config/config_page.dart';
import 'package:wool/page/config/help_page.dart';
import 'package:wool/page/web/webview_page.dart';
import 'package:wool/utils/navigator_utils.dart';
import 'package:wool/widget/item_widget.dart';

class HomePage extends StatefulWidget {
  static final String sName = "home";

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List _list = [];

  @override
  void initState() {
    super.initState();
    _init();
  }
  _init() async{
    _list = await LocalConfig.getLocalData();
    setState(() {});
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AJColors.backgroundColor,
      appBar: AppBar(
        title: Text("首页"),
        actions: [
          Container(
            width: 48,
            margin: EdgeInsets.only(right: 10),
            child: IconButton(
                icon: Icon(
                  Icons.settings,
                  color: Colors.white,
                  size: 24,
                ),
                onPressed: () {
                  NavigatorUtils.pushTO(context, child: ConfigPage()).then((v){
                    _init();
                  });
                }),
          ),
          Container(
            width: 48,
            margin: EdgeInsets.only(right: 10),
            child: IconButton(
                icon: Icon(
                  Icons.help_outline,
                  color: Colors.white,
                  size: 24,
                ),
                onPressed: () {
                  NavigatorUtils.pushTO(context, child: HelpPage());
                }),
          )
        ],
      ),
      body: _body(),
    );
  }

  _body() {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        var map = _list[index];
        return ItemWidget(
          title: "资源${index + 1}",
          desc: "${map["title"]}",
          onTap: () {
            NavigatorUtils.pushTO(context,
                child: WebViewPage(
                    navTitle: "${map["title"]}", urlStr: "${map["url"]}"));
          },
        );
      },
      itemCount: _list.length,
    );
  }
}
