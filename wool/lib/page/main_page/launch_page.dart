import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:wool/common/aj_style.dart';
import 'package:wool/page/home/home_page.dart';

class LaunchPage extends StatefulWidget {
  @override
  _LaunchPageState createState() => _LaunchPageState();
}

class _LaunchPageState extends State<LaunchPage> {
  @override
  void initState() {
    super.initState();
    _initAsync();
  }

  _initAsync() {
    Future.delayed(Duration(milliseconds: 2800)).then((_) async {
      await (new Connectivity().checkConnectivity());
      Navigator.pushReplacementNamed(context, HomePage.sName);
    });
  }

  @override
  Widget build(BuildContext context) {
    final widthSrcreen = MediaQuery.of(context).size.width;
    final heightScreen = MediaQuery.of(context).size.height;
    return new Container(
      width: widthSrcreen,
      height: heightScreen,
      color: Colors.white,
      child: Column(
        children: [
          Expanded(child: Container()),
          Image.asset(
            'assets/images/wool_logo.png',
            width: widthSrcreen * 0.5,
          ),
          Container(
            child: Text(
              "小羊VIP视频助手",
              style: AJStyle.getTextStyle(
                  color: Colors.black, fontSize: 18, isFontWeight: true),
            ),
          ),
          Expanded(child: Container()),
        ],
      ),
    );
  }
}
