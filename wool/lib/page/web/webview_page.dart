import 'dart:async';
import 'dart:io';

import 'package:aj_flutter_auto_orientation/aj_flutter_auto_orientation.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:wool/common/aj_style.dart';
import 'package:wool/page/web/parsing_page.dart';
import 'package:wool/utils/navigator_utils.dart';
import 'package:wool/utils/tools.dart';
import 'package:wool/widget/custom_appbar.dart';

class WebViewPage extends StatefulWidget {
  String navTitle;
  String urlStr;

  WebViewPage({
    @required this.urlStr,
    this.navTitle = "",
  });

  @override
  _WebViewPageState createState() => _WebViewPageState();
}

class _WebViewPageState extends State<WebViewPage> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        title: widget.navTitle,
        navigationBarBackgroundColor: AJColors.navColor,
        leadingWidget: Container(child: Row(children: [
          IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
                size: 24,
              ),
              onPressed: () {
                _controller.future.then((vc) async {
                  if (await vc.canGoBack()) {
                    await vc.goBack();
                  } else {
                    Navigator.of(context).pop();
                  }
                });
              }),
          IconButton(
              icon: Icon(
                Icons.close,
                color: Colors.white,
                size: 24,
              ),
              onPressed: (){
                Navigator.of(context).pop();
              })
        ],),),
        trailingWidget: Container(child: Row(children: [
          IconButton(
              icon: Icon(
                Icons.computer,
                color: Colors.white,
                size: 24,
              ),
              onPressed: _leading)
        ],),),
      ),
      backgroundColor: Colors.white54,
      body: _body(),
    );
  }

  _leading() async {
    var future = await _controller.future;
    var _currentUrl = await future.currentUrl();
    NavigatorUtils.pushTO(context,
        child: ParsingPage(
          navTitle: "解析",
          urlStr: "http://vip.wandhi.com?url=$_currentUrl",
        )).then((v) {
      print("-----");
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);
      AjFlutterAutoOrientation.portraitUpMode();
    });
  }

  _body() {
    return WebView(
      initialUrl: widget.urlStr,
      javascriptMode: JavascriptMode.unrestricted,
      gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
        Factory<OneSequenceGestureRecognizer>(() => EagerGestureRecognizer())
      ].toSet(),
      onWebViewCreated: (WebViewController webViewController) {
        _controller.complete(webViewController);
      },
      javascriptChannels: <JavascriptChannel>[
        _toasterJavascriptChannel(context),
      ].toSet(),
      navigationDelegate: (NavigationRequest request) {
        if (Platform.isIOS) {
          _javascriptModeH5ToNative(request.url);
        }
        return NavigationDecision.navigate;
      },
      onUrlChange: _onUrlChange,
      onPageFinished: (String url) {
        if (Platform.isAndroid) {
          _javascriptModeH5ToNative(url);
        }
      },
    );
  }

  _javascriptModeH5ToNative(String url) async {
    print(url);
  }

  _onUrlChange(String url) async {
    _controller.future.then((vc) async {
//      widget.navTitle = await vc.getTitle();
      setState(() {});
    });
    print("_onUrlChange  $url");
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'NativeJavascriptChannel',
        onMessageReceived: (JavascriptMessage message) {
          print(message.message);
        });
  }
}
