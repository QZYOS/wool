import 'dart:async';
import 'dart:io';

import 'package:aj_flutter_plugin/aj_flutter_plugin.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ParsingPage extends StatefulWidget {
  String navTitle;
  String urlStr;

  ParsingPage({
    @required this.urlStr,
    this.navTitle = "",
  });

  @override
  _ParsingPageState createState() => _ParsingPageState();
}

class _ParsingPageState extends State<ParsingPage> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(child: Container(margin: EdgeInsets.only(top: 20),child: _body(),)),
    );
  }

  _body() {
    return Stack(
      children: [
        WebView(
          initialUrl: widget.urlStr,
          javascriptMode: JavascriptMode.unrestricted,
          gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
            Factory<OneSequenceGestureRecognizer>(
                () => EagerGestureRecognizer())
          ].toSet(),
          onWebViewCreated: (WebViewController webViewController) {
            _controller.complete(webViewController);
          },
          javascriptChannels: <JavascriptChannel>[
            _toasterJavascriptChannel(context),
          ].toSet(),
          navigationDelegate: (NavigationRequest request) {
            if (Platform.isIOS) {
              _javascriptModeH5ToNative(request.url);
            }
            return NavigationDecision.navigate;
          },
          onUrlChange: _onUrlChange,
          onPageFinished: (String url) {
            if (Platform.isAndroid) {
              _javascriptModeH5ToNative(url);
            }
          },
        ),
        Positioned(
            top: 44,
            left: 20,
            child: IconButton(
                icon: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Colors.grey.withAlpha(100),
                      borderRadius: BorderRadius.all(Radius.circular(40))),
                  child: Icon(
                    Icons.arrow_back,
                    size: 30,
                    color: Colors.white,
                  ),
                ),
                onPressed: () {
                  _controller.future.then((vc) async {
                    if (await vc.canGoBack()) {
                      await vc.goBack();
                    } else {
                      Navigator.of(context).pop();
                    }
                  });
                })),
        Positioned(
            top: 44,
            right: 20,
            child: IconButton(
                icon: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Colors.grey.withAlpha(100),
                      borderRadius: BorderRadius.all(Radius.circular(40))),
                  child: Icon(
                    Icons.share,
                    size: 30,
                    color: Colors.white,
                  ),
                ),
                onPressed: () {
                  _controller.future.then((vc) async {
                    await launch(widget.urlStr);
                  });
                }))

      ],
    );
  }

  _javascriptModeH5ToNative(String url) async {
    print(url);
  }

  _onUrlChange(String url) async {
    _controller.future.then((vc) async {
      widget.navTitle = await vc.getTitle();
      setState(() {});
    });
    print("_onUrlChange  $url");
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'NativeJavascriptChannel',
        onMessageReceived: (JavascriptMessage message) {
          print(message.message);
        });
  }
}
