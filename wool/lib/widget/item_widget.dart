import 'package:flutter/material.dart';
import 'package:wool/common/aj_style.dart';

class ItemWidget extends StatelessWidget {
  final Color appLineColor;

  final String title;
  final String desc;
  final GestureTapCallback onTap;
  final IconData icon;

  ItemWidget({
    this.appLineColor,
    this.title = "",
    this.desc = "",
    this.onTap,
    this.icon = Icons.keyboard_arrow_right,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      margin: EdgeInsets.only(left: 10, right: 10, top: 10),
      padding: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(6)),
        border: Border.all(color: appLineColor ?? AJColors.app_line),
      ),
      child: InkWell(
        onTap: onTap,
        child: Row(
          children: [
            Expanded(
                child: Container(
              margin: EdgeInsets.only(left: 14),
              child: Text(
                title,
                style: AJStyle.summarizeDescStyle,
              ),
            )),
            Expanded(
                child: Container(
              alignment: Alignment.centerRight,
              child: Text(
                desc,
                style: AJStyle.summarizeDescStyle,
              ),
            )),
            Container(
              margin: EdgeInsets.only(left: 20),
              alignment: Alignment.center,
              child: Icon(icon, size: 16,),
            )
          ],
        ),
      ),
    );
  }
}
